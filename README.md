# Challenge

## The goal
Develop a web application that can authenticate in our test auth API, implementing a JWT solution.
After that is in place, use the access token returned on the login response to authenticate against our core API, and  create a solution to add/edit/delete/list clients.

## The deadline
You have two weeks to complete the challenge.  
If you are ready to deliver, you have found a bug on the API, or you have any doubt, please send and email to [bruno.pires@herzsache.com](mailto:bruno.pires@herzsache.com)  or give me a call.

## Authentication API 

The base url:  
[https://zj8s6fnvwf.execute-api.eu-central-1.amazonaws.com/prd/](https://zj8s6fnvwf.execute-api.eu-central-1.amazonaws.com/prd)  

### Signup


**Endpoint**: /signup  
**Method**: POST  
**Request**

	{
		"username":"test_user",
		"password":"123456",
		"name":"john doe",
		"email":"john.doe@herzsache.com",
		"permissions":"admin" 
	}  
**Permissions can be one of the following**  
_read_, _read\_write_, _admin_, _super\_admin_

**Response**  
_http status 200_


### Login

**Endpoint**: /login  
**Method**: POST  
**Request**

	{
		"username":"test_user",
		"password":"123456"
	} 

**Response**  

	{
	    "id": "4fcf0359-4ee1-45ad-a55a-28f440d0f4fb",
	    "created": "2019-11-02T20:28:30.069583+00:00",
	    "modified": "2019-11-04T22:57:10.1644+00:00",
	    "username": "test_user",
	    "password": "pADYcC4iRXi3MzKgSgMiQtRtBk/LLLl3ZGep50syIdg=",
	    "name":"john doe",
	    "email":"john.doe@herzsache.com",
	    "notes": null,
	    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo",
	    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
	    "token_expiration": "2019-11-04T23:04:04Z",
	    "refresh_token_expiration": "2019-11-05T00:59:04Z",
	    "permissions": "admin"
	}

### Refresh

**Endpoint**: /refresh  
**Method**: POST  
**Request**

	{
		"token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9"
		/*the refresh token*/
	}

**Response**  

	{
	    "id": "4fcf0359-4ee1-45ad-a55a-28f440d0f4fb",
	    "created": "2019-11-02T20:28:30.069583+00:00",
	    "modified": "2019-11-04T22:57:10.1644+00:00",
	    "username": "test_user",
	    "password": "pADYcC4iRXi3MzKgSgMiQtRtBk/LLLl3ZGep50syIdg=",
	    "name":"john doe",  
	    "email":"john.doe@herzsache.com",
	    "notes": null,
	    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo",
	    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
	    "token_expiration": "2019-11-04T23:04:04Z",
	    "refresh_token_expiration": "2019-11-05T00:59:04Z",
	    "permissions": "admin"
	}

## Core API 

The base url:  
[https://glvc3jkh29.execute-api.eu-central-1.amazonaws.com/prd](https://glvc3jkh29.execute-api.eu-central-1.amazonaws.com/prd)  

**Use the token obtained** on the login endpoint and use it in the Core API as Bearer Authentication. 

### Add client
**Endpoint**: /client  
**Method**: POST  
**Header**: Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo  
**Request**:

	{
		"name":"test_client",
		"notes": "test_notes"
	}

**Response - HTTP 200**  

	{
	    "id": "5e499b20-6736-4e69-accd-570a2f51f266",
	    "name": "test_client",
	    "notes": "test_notes",
	    "created": "2019-11-05T00:03:26.50008+00:00",
	    "modified": "2019-11-05T00:03:26.50008+00:00"
	}
	
**Response - HTTP 400**

	{
	    "error": [
	        "NAME_IN_USE"
	    ]
	}
	
### Update client
**Endpoint**: /client  
**Method**: PUT  
**Header**: Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo  
**Request**:

	{
		"name":"test_client",
		"notes": "test_notes_2"
	}

**Response - HTTP 200**  
_empty_
	
**Response - HTTP 400**

	{
	    "error": [
	        "NAME_IN_USE"
	    ]
	}

### Delete client
**Endpoint**: /client/<ID>  
**Method**: DELETE  
**Header**: Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo  


**Response - HTTP 200**  
_empty_

### List clients
**Endpoint**:  /client  
**Method**:  GET  
**Header**: Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJo 


**Response**  

	[
		{
		    "id": "5e499b20-6736-4e69-accd-570a2f51f266",
		    "name": "test_client",
		    "notes": "test_notes",
		    "created": "2019-11-05T00:03:26.50008+00:00",
		    "modified": "2019-11-05T00:03:26.50008+00:00"
		},
			{
		    "id": "5e499b20-6736-4e69-accd-DSJH87f51f266",
		    "name": "test_client2",
		    "notes": "test_notes2",
		    "created": "2019-11-05T00:03:26.50008+00:00",
		    "modified": "2019-11-05T00:03:26.50008+00:00"
		}
	]
	
	
## Extra
Finding and reporting bugs on the API is a plus 🐛🐛🐛